import React from 'react'
import Play from 'ui/screens/Play/Play.jsx'

const App = () => (
    <React.StrictMode>
        <Play />
    </React.StrictMode>
)

export default App
