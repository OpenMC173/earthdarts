import React, { useEffect, useRef } from "react"

const StartMenu = () => {

    const thisDialog = useRef()

    useEffect(() => {
        if (!thisDialog.current.open) {
            thisDialog.current.showModal()
        }
    }, [])

    return (
        <dialog ref={thisDialog}>
            <header>
                <h1>EarthDarts</h1>
            </header>
            <div>
                <button onClick={() => thisDialog.current.close()}>Play</button>
            </div>
        </dialog>
    )
}

export default StartMenu
