import { Loader } from "@googlemaps/js-api-loader"
import React, { useEffect, useRef } from "react"
import StartMenu from "ui/dialogs/StartMenu.jsx"

const Play = () => {

    const mapElement = useRef()

    useEffect(() => {
        const loader = new Loader({
            apiKey: "AIzaSyAG1b45A2pxPZaHTNy7WLUj3tNg6rIYz2c",
            version: "beta",
            libraries: ["places"],
        })

        loader.load()
            .then((google) => {
                new google.maps.Map(mapElement.current, {
                    center: { lat: 0, lng: 0 },
                    zoom: 2,
                    mapTypeId: "satellite",
                    // mapTypeControl: false,
                    isFractionalZoomEnabled: true,
                    tilt: false,
                    styles: [
                        { featureType: "poi", stylers: [{ visibility: "off" }] },
                        { featureType: "transit", stylers: [{ visibility: "off" }] },
                        { featureType: "road", elementType: "labels", stylers: [{ visibility: "off" }] },
                        { featureType: "administrative", elementType: "labels", stylers: [{ visibility: "off" }] },
                        { featureType: "landscape.man_made", stylers: [{ visibility: "off" }] },
                        { featureType: "water", elementType: "labels", stylers: [{ visibility: "off" }] },
                        { featureType: "landscape", elementType: "labels", stylers: [{ visibility: "off" }] },
                    ]
                })
            })
            .catch((err) => {
                console.error(err)
            })
    }, [])

    return (
        <>
            <div ref={mapElement} style={{ height: "100%" }} />
            <StartMenu />
        </>
    )
}

export default Play
